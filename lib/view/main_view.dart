import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_routine_app/provider.dart';
import 'package:flutter_routine_app/view/create_routine.dart';
import 'package:flutter_routine_app/collections/routine.dart';
import 'package:flutter_routine_app/view/update_routine.dart';
import 'package:isar/isar.dart';

class MainView extends ConsumerStatefulWidget {
  const MainView({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _MainViewState();
}

class _MainViewState extends ConsumerState<MainView> {
  List<Routine> routines = [];
  final _searchController = TextEditingController();
  String feedback = "";
  bool _searching = false;
  @override
  void initState() {
    Future.delayed(Duration.zero, () => _getRoutine());
    createWatcher();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: const Text("Routine", style: TextStyle(color: Colors.black)),
        actions: [
          IconButton(
            style: IconButton.styleFrom(
              alignment: Alignment.center,
              shape: const CircleBorder(
                side: BorderSide(
                  color: Colors.black,
                ),
              ),
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CreateRoutineView(),
                  ));
            },
            icon: const Icon(
              Icons.add,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: _searchController,
                onChanged: (value) => _searchByName(value),
                decoration: InputDecoration(
                  hintText: "Search... ",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                    borderSide: const BorderSide(style: BorderStyle.solid),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  feedback,
                  style: const TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            FutureBuilder(
                future: _getRoutine(),
                builder: (context, builder) {
                  return ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: routines.length,
                    itemBuilder: (context, index) => Card(
                      elevation: 4.0,
                      margin: const EdgeInsets.all(4),
                      child: ListTile(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => UpdateRoutineView(
                                    routine: routines[index])),
                          );
                        },
                        title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 5.0, bottom: 2.0),
                                child: Text(
                                  routines[index].title,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 5.0),
                                child: RichText(
                                    text: TextSpan(
                                        style: const TextStyle(
                                            color: Colors.black),
                                        children: [
                                      const WidgetSpan(
                                          child:
                                              Icon(Icons.schedule, size: 16)),
                                      TextSpan(text: routines[index].startTime)
                                    ])),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 5.0),
                                child: RichText(
                                    text: TextSpan(
                                        style: const TextStyle(
                                            color: Colors.black, fontSize: 12),
                                        children: [
                                      const WidgetSpan(
                                          child: Icon(
                                        Icons.calendar_month,
                                        size: 16,
                                      )),
                                      TextSpan(text: routines[index].day)
                                    ])),
                              )
                            ]),
                        trailing: const Icon(Icons.keyboard_arrow_right),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SizedBox(
          height: 50,
          child: ElevatedButton(
            onPressed: () {
              clearAll();
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.grey[400],
              textStyle: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
              foregroundColor: Colors.white,
            ),
            child: const Text("Clear All"),
          ),
        ),
      ),
    );
  }

  Future _getRoutine() async {
    if (!_searching) {
      final routineCollection = ref.read(isarProvider).routines;
      routines = await routineCollection.where().findAll();
      setState(() {});
    }
  }

  _searchByName(String value) async {
    if (value.isEmpty) {
      _searching = false;
      _getRoutine();
    } else {
      _searching = true;
      final routineCollection = ref.read(isarProvider).routines;
      routines.clear();
      routines =
          await routineCollection.filter().titleContains(value).findAll();
      setState(() {});
    }
  }

  void clearAll() {
    final routineCollection = ref.read(isarProvider).routines;
    ref.read(isarProvider).writeTxn(() async {
      routineCollection.clear();
    });
  }

  createWatcher() async {
    final Query<Routine> tasks =
        ref.read(isarProvider).routines.where().build();
    Stream<List<Routine>> queryChanged = tasks.watch(fireImmediately: true);
    queryChanged.listen((List<Routine> routines) {
      if (routines.length > 1) {
// Do something
        setState(() {
          feedback = "Great job";
        });
      } else {
        // Do something
        setState(() {
          feedback = "Need more";
        });
      }
    });
  }
}
