import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_routine_app/collections/category.dart';
import 'package:flutter_routine_app/collections/routine.dart';
import 'package:flutter_routine_app/provider.dart';
import 'package:isar/isar.dart';

class CreateRoutineView extends ConsumerStatefulWidget {
  const CreateRoutineView({super.key});
  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _CreateRoutineViewState();
}

class _CreateRoutineViewState extends ConsumerState<CreateRoutineView> {
  List<Category> categories = [];
  List<String> days = [
    'saturaday',
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
  ];
  final _titleController = TextEditingController();
  final _timeController = TextEditingController();
  final _newCategoryController = TextEditingController();
  Category? categoryValue;
  String? dayValue = 'saturaday';
  TimeOfDay selectedTime = TimeOfDay.now();
  @override
  void initState() {
    _readCategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Routine"),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            right: 20.0,
            left: 20.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Category",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: size.width * 0.7,
                    child: DropdownButton(
                      value: categoryValue ?? null,
                      isExpanded: true,
                      elevation: 8,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      items: categories
                          .map(
                            (value) => DropdownMenuItem<Category>(
                              value: value,
                              child: Text(value.name),
                            ),
                          )
                          .toList(),
                      onChanged: (Category? newValue) {
                        setState(() {
                          categoryValue = newValue;
                        });
                        debugPrint("======${categoryValue?.name}");
                      },
                    ),
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text("New Category"),
                              content: TextFormField(
                                controller: _newCategoryController,
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    if (_newCategoryController
                                        .text.isNotEmpty) {
                                      _createNewCategory();
                                      Navigator.pop(context);
                                    }
                                  },
                                  child: const Text(
                                    'Add',
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                )
                              ],
                            );
                          });
                    },
                    style: IconButton.styleFrom(
                      alignment: Alignment.center,
                      shape: const CircleBorder(
                        side: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                    ),
                    icon: const Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 8),
              const Text("Title"),
              TextField(
                controller: _titleController,
              ),
              const SizedBox(height: 10),
              const Text("Start Time"),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: _timeController,
                      enabled: false,
                    ),
                  ),
                  IconButton(
                    onPressed: () => _selectedTime(context),
                    icon: const Icon(
                      Icons.calendar_month,
                      color: Colors.black87,
                    ),
                  )
                ],
              ),
              const SizedBox(height: 8),
              const Text("Day"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: size.width * 0.7,
                    child: DropdownButton(
                      value: dayValue,
                      isExpanded: true,
                      elevation: 8,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      items: days
                          .map(
                            (value) => DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            ),
                          )
                          .toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          dayValue = newValue ?? "";
                        });
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              Align(
                child: SizedBox(
                  width: size.width / 2,
                  child: ElevatedButton(
                    onPressed: () {
                      _addRoutine();
                    },
                    style: ElevatedButton.styleFrom(
                      elevation: 5,
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.white,
                    ),
                    child: const Text(
                      "Add",
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectedTime(BuildContext context) async {
    final TimeOfDay? timeOfDay = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        initialEntryMode: TimePickerEntryMode.dial);

    if (timeOfDay != null && timeOfDay != selectedTime) {
      selectedTime = timeOfDay;
      setState(() {
        _timeController.text =
            "${selectedTime.hour}:${selectedTime.minute} ${selectedTime.period.name}";
      });
    }
  }

  //create category record

  _createNewCategory() async {
    final categories = ref.read(isarProvider).categorys;

    final newCategory = Category()..name = _newCategoryController.text;

    await ref
        .read(isarProvider)
        .writeTxn(() async => await categories.put(newCategory));

    _newCategoryController.clear();
    _readCategories();
  }

  _readCategories() async {
    final categoriesCollection =
        await ref.read(isarProvider).categorys.where().findAll();

    setState(() {
      categories = categoriesCollection;
    });
  }

  @override
  void dispose() {
    _newCategoryController.dispose();
    _timeController.dispose();
    _titleController.dispose();
    super.dispose();
  }

  void _addRoutine() async {
    final newRoutine = Routine()
      ..title = _titleController.text
      ..startTime = _timeController.text
      ..day = dayValue ?? ""
      ..category.value = categoryValue;

    final isar = ref.read(isarProvider);

    ///  Insert data to routine database via [ Isar.writeTxn() ] method write transactions method
    isar.writeTxnSync(() => isar.routines.putSync(newRoutine));
    _titleController.clear();
    final selectedTime = TimeOfDay.now();
    _timeController.text =
        "${selectedTime.hour}:${selectedTime.minute} ${selectedTime.period.name}";
    setState(() {
      dayValue = days[0];
      categoryValue = null;
    });
  }
}
