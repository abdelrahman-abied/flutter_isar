import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_routine_app/collections/category.dart';
import 'package:flutter_routine_app/collections/routine.dart';
import 'package:flutter_routine_app/provider.dart';
import 'package:flutter_routine_app/view/main_view.dart';
import 'package:isar/isar.dart';

class UpdateRoutineView extends ConsumerStatefulWidget {
  final Routine routine;
  const UpdateRoutineView({required this.routine, super.key});
  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _UpdateRoutineViewState();
}

class _UpdateRoutineViewState extends ConsumerState<UpdateRoutineView> {
  List<Category> categories = [];
  List<String> days = [
    'saturaday',
    'sunday',
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
  ];
  final _titleController = TextEditingController();
  final _timeController = TextEditingController();
  final _newCategoryController = TextEditingController();
  Category? categoryValue;
  String? dayValue = 'saturaday';
  TimeOfDay selectedTime = TimeOfDay.now();
  @override
  void initState() {
    loadRoutine();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.routine.title),
        actions: [
          IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                        title: const Text("Delete Routine"),
                        content: const Text("Are you sure to delete routine"),
                        actions: [
                          TextButton(
                            onPressed: () {
                              deleteRoutine();
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: ((context) => const MainView())),
                                  (route) => false);
                            },
                            child: const Text(
                              "OK",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: const Text("Cancel"),
                          )
                        ],
                      ));
            },
            icon: Icon(Icons.delete, color: Colors.red[400]),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            right: 20.0,
            left: 20.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Category",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  if (categories.isNotEmpty)
                    SizedBox(
                      width: size.width * 0.7,
                      child: DropdownButton<Category>(
                        value: categoryValue,
                        isExpanded: true,
                        elevation: 8,
                        icon: const Icon(Icons.keyboard_arrow_down),
                        items: categories
                            .map(
                              (value) => DropdownMenuItem<Category>(
                                value: value,
                                child: Text(value.name),
                              ),
                            )
                            .toSet()
                            .toList(),
                        onChanged: (Category? newValue) {
                          setState(() {
                            categoryValue = newValue;
                          });
                        },
                      ),
                    ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text("New Category"),
                              content: TextFormField(
                                controller: _newCategoryController,
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    if (_newCategoryController
                                        .text.isNotEmpty) {
                                      _createNewCategory();
                                      Navigator.pop(context);
                                    }
                                  },
                                  child: const Text(
                                    'Add',
                                    style: TextStyle(color: Colors.black87),
                                  ),
                                )
                              ],
                            );
                          });
                    },
                    style: IconButton.styleFrom(
                      alignment: Alignment.center,
                      shape: const CircleBorder(
                        side: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                    ),
                    icon: const Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 8),
              const Text("Title"),
              TextField(
                controller: _titleController,
              ),
              const SizedBox(height: 10),
              const Text("Start Time"),
              Row(
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: _timeController,
                      enabled: false,
                    ),
                  ),
                  IconButton(
                    onPressed: () => _selectedTime(context),
                    icon: const Icon(
                      Icons.calendar_month,
                      color: Colors.black87,
                    ),
                  )
                ],
              ),
              const SizedBox(height: 8),
              const Text("Day"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: size.width * 0.7,
                    child: DropdownButton(
                      value: dayValue ?? null,
                      isExpanded: true,
                      elevation: 8,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      items: days
                          .map(
                            (value) => DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            ),
                          )
                          .toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          dayValue = newValue ?? "";
                        });
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              Align(
                child: SizedBox(
                  width: size.width / 2,
                  child: ElevatedButton(
                    onPressed: () {
                      _updateRoutine();
                    },
                    style: ElevatedButton.styleFrom(
                      elevation: 5,
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.white,
                    ),
                    child: const Text(
                      "Update",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectedTime(BuildContext context) async {
    final TimeOfDay? timeOfDay = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        initialEntryMode: TimePickerEntryMode.dial);

    if (timeOfDay != null && timeOfDay != selectedTime) {
      selectedTime = timeOfDay;
      setState(() {
        _timeController.text =
            "${selectedTime.hour}:${selectedTime.minute} ${selectedTime.period.name}";
      });
    }
  }

  //create category record

  _createNewCategory() async {
    final categories = ref.read(isarProvider).categorys;

    final newCategory = Category()..name = _newCategoryController.text;

    await ref
        .read(isarProvider)
        .writeTxn(() async => await categories.put(newCategory));
    _newCategoryController.clear();
    categories.clear();
    categoryValue = null;
    _readCategories();
  }

  _readCategories() async {
    final categoriesCollection =
        await ref.read(isarProvider).categorys.where().findAll();

    categoriesCollection.forEach((element) => print(element.name));

    categories = categoriesCollection;
    setState(() {});
  }

  @override
  void dispose() {
    _newCategoryController.dispose();
    _timeController.dispose();
    _titleController.dispose();
    super.dispose();
  }

  void _updateRoutine() async {
    final routineCollection = ref.read(isarProvider).routines;
    final routine = await routineCollection.get(widget.routine.id);

    ///  Insert data to routine database via [ Isar.writeTxn() ] method write transactions method
    await ref.read(isarProvider).writeTxnSync(() async {
      print("=======${categoryValue?.name}");
      routine!
        ..title = _titleController.text
        ..startTime = _timeController.text
        ..day = dayValue ?? ""
        ..category.value = categoryValue!;
      routineCollection.putSync(routine);
    });
    Navigator.pop(context);
  }

  void loadRoutine() async {
    _readCategories();
    await widget.routine.category.load();
    int? categoryId = widget.routine.category.value?.id;
    _timeController.text = widget.routine.startTime;
    _titleController.text = widget.routine.title;
    _newCategoryController.text = widget.routine.category.value?.name ?? "";
    dayValue = widget.routine.day;
    print("=======${categoryId}");

    setState(() {
      categoryValue = categories[categoryId! - 1];
    });
  }

  void deleteRoutine() async {
    final isar = ref.read(isarProvider);
    final routineCollection = ref.read(isarProvider).routines;

    await isar
        .writeTxn(() async => routineCollection.delete(widget.routine.id));
  }
}
